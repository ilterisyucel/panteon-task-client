import React from 'react';
//import './App.css';
import { Grid, GridColumn } from '@progress/kendo-react-grid';
import '@progress/kendo-theme-default/dist/all.css';

class App extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      playerList : []
    }
  }
  componentDidMount(){
    fetch('https://warm-savannah-32238.herokuapp.com/get-players?rank=120').then(response => {  
      if(response.status !== 200){
        console.log('Player data cannot be getted.');
        throw new Error('Unreached Data');
      }
      return response.json();
    }).then(data => {
      data.forEach((player, index) => {
        player['rank'] = index+1;
        player['dailydiff'] = 0;
      });
      this.setState({playerList:data});
    })
  }

  render() {
    return (
      <div className="App">
        <Grid
          data={this.state.playerList}
        >
          <GridColumn field='rank' title='RANK'/>
          <GridColumn field='username' title='PLAYER NAME'/>
          <GridColumn field='country' title='COUNTRY'/>
          <GridColumn field='money' title='MONEY'/>
          <GridColumn field='dailydiff' title='DAILY DIFF'/>
        </Grid>
      </div>
    );
  }
}

export default App;
